/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,ts,jsx,tsx,mdx}"
  ],
  theme: {
    extend: {
      fontFamily:{
        sarsabs:"Plus Jakarta Sans",
        serif:"sans-serif"
      }
      ,
      screens: {
        mob: {'max': '639px'},
        // => @media (max-width: 639px) { ... }
      },
      colors:{
        main:{
          light:"#fff"
        },

        secandry:{
          light:"#ff3951"
        },
        panel:{
          light:"#14142bcc"
        },
        background:{
          from:"#ff395147",
          review:"#f5f7fc"
        }
      },
      dropShadow: {
        '2xl': '0 35px 35px rgba(255, 57, 81, 0.28)',
        'xl': '0 25px 25px rgba(255, 57, 81, 0.28)',
        '4xl': [
            '0 35px 35px rgba(0, 0, 0, 0.25)',
            '0 45px 65px rgba(0, 0, 0, 0.15)'
        ]
    },
      maxWidth: {
      'lg': '27rem',
    },
    width:{
     custom:"30.66%",
     customS:"59.66%",
     half:"45.22%",
     sec:"96.0vw",
     secads:"98.0vw",
     ads:"98.6vw"
    }
  },
  plugins: [],
}
}
