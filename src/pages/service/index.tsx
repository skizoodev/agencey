import React from 'react'
import Navbar from '@/components/navbar'
import  Ads  from '@/components/ads'
import Header from '@/components/service/header'
import Service from "@/components/service"
export default function Services() {
  return (
    <>
    <Ads/>
    <Navbar/>
    <Header/>
    <Service/>
    </>
  )
}
