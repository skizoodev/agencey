import React from 'react'
import Navbar from '@/components/navbar'
import Ads from '@/components/ads'
import Header from '@/components/service/website/header'
import Contant from '@/components/service/website/contant'
import State from '@/components/service/website/state'
import Dev from '@/components/service/website/secandB'
import Service from '@/components/service/website/service'
import Footer from '@/components/footer'
import Hire from '@/components/service/website/hire'
import Price from '@/components/service/website/price'
export default function website() {
  return (
    <>
    <Ads/>
      <Navbar/>
      <Header/>
      <State/>
      <Contant/>
      <Service/>
      <Dev/>
      <Price/>
      <Hire/>
      <Footer/>
    </>
  )
}
