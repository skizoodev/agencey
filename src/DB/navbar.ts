export const navigation=[
    { 
    name: "Home", 
    href: "/",
    children:[
        {
            name:"about",
            href:"#about",
            icon:"/asset/home/about.png",
            att:"Freepik"
    },
    {
        name:"service",
        href:"#service",
        icon:"/asset/home/service.png",
        att:"Eucalyp"
},
    {
        name:"plan",
        href:"#plan",
        icon:"/asset/home/plan.png",
        att:"Aficons studio"
},
{
    name:"testimonial",
    href:"#testimonial",
    icon:"/asset/home/review.png",
    att:"Freepik"
}
    ] 
},
{ 
    name: "Services", 
    href: "/service",
    children:[
        {
            name:"Consulting ",
            href:"/service/consulting ",
            icon:"/asset/home/cs.png",
            att:"Freepik"
    },
    
    {
        name:"Website",
        href:"/service/website",
        icon:"/asset/home/dev.png",
        att:"Sicon"
},
        {
            name:"Marketing",
            href:"/service/markting",
            icon:"/asset/home/ms.png",
            att:"Freepik"
    }

    ] 
},
{
    name: "Packages", 
    href: "/packages",
    children:[
        {
            name:"about",
            href:"#about",
            icon:"/asset/home/about.png",
            att:"Freepik"
    },
    {
        name:"Service",
        href:"#service",
        icon:"/asset/home/service.png",
        att:"Eucalyp"
}]
},
{
    name: "Contact", 
    href: "/contact",
    children:[
        {
            name:"about",
            href:"#about",
            icon:"/asset/home/about.png",
            att:"Freepik"
    },
    {
        name:"service",
        href:"#service",
        icon:"/asset/home/service.png",
        att:"Eucalyp"
}]
}
]