
export const ServiceInfo = [
    {
        id: 1,
        idd: 1,
        logo: "/asset/service/paid.svg",
        title: "paid advertising",
        text: "Unlock the full potential of your business with our professional Paid Advertising services",
        okicon: "/asset/service/paidok.svg",
        benefit: ["meta ads", "google ads", "tiktok ads"]
    },
    {
        id: 2,
        idd: 2,
        logo: "/asset/service/content.svg",
        title: "Content Marketing",
        text: "Engage and convert your audience with powerful,relevant content that fuels your business growth.",
        okicon: "/asset/service/contentok.svg",
        benefit: ["copywriting", "writing content", "infographics"]
    },
    {
        id: 3,
        idd: 3,
        logo: "/asset/service/funnel.svg",
        title: "Funnel Optimization",
        text: " Convert More, Succeed Faster elevate your marketing funnel with our data-driven strategies",
        okicon: "/asset/service/funnelok.svg",
        benefit: ["converstion optimization", "analytics analisis", "A/B Testing"]
    },

    {
        id: 4,
        idd: 4,
        logo: "/asset/service/social.svg",
        title: "Social Media Growth",
        text: " become an online influence and captivate your audience with our cutting-edge strategies",
        okicon: "/asset/service/socialok.svg",
        benefit: ["followers ", "engagement", "views"]
    },
    {
        id: 5,
        idd: 5,
        logo: "/asset/service/webm.svg",
        title: "Website Development",
        text: "Unlock the full potential of your business with our expert website development services",
        okicon: "/asset/service/emailOk.svg",
        benefit: ["modern technology", "custom designs", "Free deployment"]
    },
    {
        id: 6,
        idd: 6,
        logo: "/asset/service/seo.svg",
        title: "SEO",
        text: "Amplify your online visibility and dominate search engine rankings  with our cutting-edge SEO strategies",
        okicon: "/asset/service/seook.svg",
        benefit: ["organic traffic", "Visibility & Credibility", "Audit SEO"]
    }
]
export const ProcessInfo = [
    {
        id: 1,
        logo: "/asset/service/talk.png",
        name: "Understanding Your Concerns",
        title: "Attentive Support",
        text: " our agency is dedicated to Understanding to your concerns and providing the necessary assistance for resolving the issue.",
    },
    {
        id: 2,
        logo: "/asset/service/target.png",
        name: "Crafting Plans",
        title: "Personalized Solutions",
        text: "We develop tailored plans and present compelling offers to meet your specific needs.",
 },
    {
        id: 3,
        logo: "/asset/service/suive.png",
        name: "Turning Vision into Reality",
        title: "Execution",
        text: "With meticulous execution and a dedicated team, we turn ideas into remarkable achievements",
    }
    
]
export const TestimonialData = [
    {
        id: 1,
        logo: "/asset/testimonial/visonet.png",
        title: "Reviews and Sales",
        company:"Founder of Viso-Net Boutique",
        name:"Nouel Lebrun ",
         text: "Working with SarSabs Marketing Agency has been a game-changer for our business! Their creative strategies and innovative ideas helped us reach new heights in the market. We highly recommend SarSabs Marketing Agency to any business looking for exceptional marketing solutions.",
    },
    {
        id: 2,
        logo: "/asset/testimonial/cedars.png",
        title: "SEO AUDIT",
        company:"Clinical Research Associate II | Cedars-Sinai.",
        name:"Suki Lee, MPHView Suki Lee, MPH’s profile ",
        text: "We were struggling to make our online presence felt until we partnered with SarSabs Marketing Agency. The results were astounding! Thanks to SarSabs Marketing Agency, we now have a solid digital foundation that propels our business forward!",
    },
    {
        id: 3,
        logo: "/asset/testimonial/case.png",
        title: "Amazon Sales",
        company:"Co-founder at Case Mate.",
        name:"Jerusalem",
        text: "As a startup, we knew the importance of effective marketing but had no idea where to start. That's when we found SarSabs Marketing Agency, and it turned out to be the best decision we ever made! We couldn't be happier with the results and are grateful for the partnership!",
    }
]
export const PackageInfo = [
    {
        id: 1,
        logo: "/asset/pack1.png",
        prix: "1000 USD",
        title: "stander",
        text: "lorem ipsum ksmql 3asba lik ya jf ;clld mxop ppps llldùsld ,f,ld,fd ",
        okicon: "/asset/service/email.svg",
        benefit: ["first Benfit", "secand Benfit", "third Benfit"]
    },
    {
        id: 2,
        logo: "/asset/pack1.png",
        prix: "1000 USD",
        title: "stander",
        text: "lorem ipsum ksmql",
        okicon: "/asset/service/email.svg",
        benefit: ["first Benfit", "secand Benfit", "third Benfit"]
    }
]
export const Ads = [
    {
        id: 1,
        text: "have qustion talk to an expert it's free"
    },
    {
        id: 2,
        text: "BUY NOW & PAY IN 3 INSTALLMENTS"
    },
    {
        id: 3,
        text: "Enjoy our 30% discount offer exclusively for doctors in Kwait! "
    },
]
export const Abouts = [
    {
        id: 1,
        text: "Guaranteed Results"
    },
    {
        id: 2,
        text: "10+ Years of Experience"
    },
    {
        id: 3,
        text: "Team of Industry Experts"
    },
    {
        id: 4,
        text: "Focus on your core business activities"
    },
    {
        id: 4,
        text: " Rest assured"
    }
   
]
export const HeroImg = [
    {
        id: 1,
        img: "/asset/home/s1.jpg"
    },
    {
        id: 2,
        img: "/asset/home/s2.jpeg"
    },
    {
        id: 3,
        img: "/asset/home/s3.png"
    },
]