import Image from 'next/image'

import { useKeenSlider, KeenSliderPlugin } from "keen-slider/react"
import {ServiceInfo} from "@/DB/main"
const carousel: KeenSliderPlugin = (slider) => {
  const z = 300
  function rotate() {
    const deg = 360 * slider.track.details.progress
    slider.container.style.transform = `translateZ(-${z}px) rotateY(${-deg}deg)`
  }
  slider.on("created", () => {
    const deg = 360 / slider.slides.length
    slider.slides.forEach((element, idx) => {
      element.style.transform = `rotateY(${deg * idx}deg) translateZ(${z}px)`
    })
    rotate()
  })
  slider.on("detailsChanged", rotate)
}
export default function Mb() {
    const [sliderRef] = useKeenSlider<HTMLDivElement>(
      {
        loop: true,
        selector: ".carousel__cell",
        renderMode: "custom",
        mode: "free-snap",
      },
      [carousel]
    )
  return (
    <div className="scene">
    <div className="carousel keen-slider" ref={sliderRef}>
{
ServiceInfo.map((s)=>(
<div key={s.idd} className='carousel__cell rounded-3xl pt-12  pb-14 pl-8 mb-9 bg-white  shadow-2xl'>
      <Image className='rounded-3xl mb-5' alt='icon' width={100} height={100} src={s.logo} />
      <div>
  <h2  className=" titleha">
        {s.title}
      </h2>
      <p className='psars'>
      {s.text}
      </p>

  </div>
  {s.benefit.map((ar:any)=>(
<div className='flex mt-4 mb-3' key={Math.random()}>
<Image  src={s.okicon} height={30} width={30} alt="ok"/>
<h6  className='pl-1 capitalize text-xl font-normal text-gray-500'>{ar}</h6>
</div>
  ))}
 </div>
))
}    </div>


</div>  )
}
