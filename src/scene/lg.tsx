import Image from 'next/image'
import {ServiceInfo} from "@/DB/main"
export default function Lg() {
  return (
        <div className="flex mt-10 flex-wrap justify-center w-full space-x-5" >
    {
    ServiceInfo.map((s:any)=>(
    <div key={s.id} className=' rounded-3xl lg:w-custom pt-12 pr-8 pb-14 pl-8 mb-9 bg-white  shadow-2xl'>
          <Image className='rounded-3xl mb-4' alt='icon' width={100} height={100} src={s.logo} />
    
          <div>
      <h2  className="titleha">
            {s.title}
          </h2>
          <p className='psars'>
          {s.text}
          </p>
    
      </div>
      {s.benefit.map((ar:any)=>(
    <div className='flex mt-4' key={Math.random()}>
    <Image  src={s.okicon} height={30} width={30} alt="ok"/>
    <h6  className='pl-3  capitalize text-2xl font-normal text-gray-500'>{ar}</h6>
    </div>
      ))}
     </div>
    ))
    }    </div>
        )
}
