import Image from "next/image"
import { useState, useEffect, Fragment } from "react"
import { SMMService } from "../DB/service"
import { useKeenSlider } from "keen-slider/react"

export default function Service() {
  const mob =
  {
    slides: {
      perView: 1,
      spacing: 2,
    },
    loop: true
  }
  const [deviceWidth, setDeviceWidth] = useState(0);

  useEffect(() => {
    const handleResize = () => {
      setDeviceWidth(window.innerWidth);
    };

    // Initial device width
    setDeviceWidth(window.innerWidth);

    // Add event listener for window resize
    window.addEventListener('resize', handleResize);

    // Clean up the event listener on component unmount
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [deviceWidth]);

  const [ref] = useKeenSlider<HTMLDivElement>({
    slides: {
      perView: 4,
      spacing: 5,
    },
    loop: true
  }, [
    (slider) => {
      let timeout: ReturnType<typeof setTimeout>
      let mouseOver = false
      function clearNextTimeout() {
        clearTimeout(timeout)
      }
      function nextTimeout() {
        clearTimeout(timeout)
        if (mouseOver) return
        timeout = setTimeout(() => {
          slider.next()
        }, 2000)
      }
      slider.on("created", () => {
        slider.container.addEventListener("mouseover", () => {
          mouseOver = true
          clearNextTimeout()
        })
        slider.container.addEventListener("mouseout", () => {
          mouseOver = false
          nextTimeout()
        })
        nextTimeout()
      })
      slider.on("dragStarted", clearNextTimeout)
      slider.on("animationEnded", nextTimeout)
      slider.on("updated", nextTimeout)
    }
  ]
  )
  const [ref1] = useKeenSlider<HTMLDivElement>(mob, [
    (slider) => {
      let timeout: ReturnType<typeof setTimeout>
      let mouseOver = false
      function clearNextTimeout() {
        clearTimeout(timeout)
      }
      function nextTimeout() {
        clearTimeout(timeout)
        if (mouseOver) return
        timeout = setTimeout(() => {
          slider.next()
        }, 2000)
      }
      slider.on("created", () => {
        slider.container.addEventListener("mouseover", () => {
          mouseOver = true
          clearNextTimeout()
        })
        slider.container.addEventListener("mouseout", () => {
          mouseOver = false
          nextTimeout()
        })
        nextTimeout()
      })
      slider.on("dragStarted", clearNextTimeout)
      slider.on("animationEnded", nextTimeout)
      slider.on("updated", nextTimeout)
    },
  ])

  return (
    <Fragment>
      {deviceWidth >= 690 ?
        SMMService.map((fb: any) => (
          <div key={Math.random()}>
            <div className="lg:mx-5 lg:pt-12 pb-5 lg:px-24 xl:px-0 flex justify-center items-center flex-col">
              <div className="flex justify-between bg-gray-50 items-stretch flex-row">
                <div className="flex items-center bg-gray-800 justify-center">
                  <p className="transform flex flex-shrink-0 -rotate-90 text-2xl font-semibold tracking-wide leading-normal text-white">50% OFF</p>
                </div>
                <div className="flex justify-center items-start flex-col xl:w-2/5 md:w-5/12 xl:px-7 px-6 md:px-0 md:py-0 py-5">
                  <div>
                    <p className="text-3xl xl:text-4xl font-semibold leading-9 text-gray-800">{fb.name}</p>
                  </div>
                  <div className="xl:mt-4 mt-2">
                    <p className="text-base xl:text-xl leading-7 text-gray-600 pr-4">{fb.description}</p>
                  </div>
                </div>
                <div className="hidden md:block h-44 md:h-60 xl:h-72">
                  <Image className="hidden h-full xl:block" src={fb.banner} alt={fb.att} width={250} height={50} />
                </div>
              </div>
            </div>
            {/**top banner bottom card */}

            <div className="px-2 flex flex-wrap justify-center items-start ">
              <div ref={ref} className="keen-slider">

                {fb.packs.map((srv: any) => (
                  <div key={srv.name} className="keen-slider__slide">
                    <div className={`flex-shrink-0 m-6 relative  overflow-hidden 
   rounded-lg max-w-xs shadow-lg ${srv.col}`}>
                      <svg
                        className="absolute bottom-0 left-0 mb-8"
                        viewBox="0 0 375 283"
                        fill="none"
                        style={{ transform: "scale(1.5)", opacity: "0.1" }}
                      >
                        <rect
                          x="159.52"
                          y={175}
                          width={152}
                          height={152}
                          rx={8}
                          transform="rotate(-45 159.52 175)"
                          fill="white"
                        />
                        <rect
                          y="107.48"
                          width={152}
                          height={152}
                          rx={8}
                          transform="rotate(-45 0 107.48)"
                          fill="white"
                        />
                      </svg>
                      <div className="relative pt-10 px-10 flex items-center justify-center">
                        <div
                          className="block absolute w-48 h-48 bottom-0 left-0 -mb-24 ml-3"
                          style={{
                            background: "radial-gradient(black, transparent 60%)",
                            transform: "rotate3d(0, 0, 1, 20deg) scale3d(1, 0.6, 1)",
                            opacity: "0.2"
                          }}
                        />
                        <Image width="100" height='100'
                          className="relative w-40"
                          src={srv.img}
                          alt={srv.att}
                        />
                      </div>
                      <div className="relative text-white px-6 pb-6 mt-6">
                        <div className="flex justify-between">
                          <span className="block opacity-75 -mb-1 uppercase text-xl">{srv.name}</span>
                          <span className="block font-semibold text-xl">{srv.result}</span>
                        </div>
                        <div className={`bg-white rounded-full text-center ${srv.text} text-xl font-bold px-3  py-2 leading-none flex items-center`}>
                          {srv.prix}
                        </div>
                        <button className={`w-full rounded-2xl cursor-help shadow-2xl text-center text-2xl font-bold  uppercase mt-3 p-2
     bg-yellow-50 ${srv.text}`}>
                          buy
                        </button>
                      </div>
                    </div>

                  </div>
                ))}
              </div>
            </div></div>

        ))
        :
        SMMService.map((fb: any) => (
          <div key={Math.random()}>
            <div className="lg:mx-5 lg:pt-12 pb-5 lg:px-24 xl:px-0 flex justify-center items-center flex-col">
              <div className="flex justify-between bg-gray-50 items-stretch flex-row">
                <div className="flex items-center bg-gray-800 justify-center">
                  <p className="transform flex flex-shrink-0 -rotate-90 text-2xl font-semibold tracking-wide leading-normal text-white">50% OFF</p>
                </div>
                <div className="flex justify-center items-start flex-col xl:w-2/5 md:w-5/12 xl:px-7 px-6 md:px-0 md:py-0 py-5">
                  <div>
                    <p className="text-3xl xl:text-4xl font-semibold leading-9 text-gray-800">{fb.name}</p>
                  </div>
                  <div className="xl:mt-4 mt-2">
                    <p className="text-base xl:text-xl leading-7 text-gray-600 pr-4">{fb.description}</p>
                  </div>
                </div>
                <div className="hidden md:block h-44 md:h-60 xl:h-72">
                  <Image className="hidden h-full xl:block" src={fb.banner} alt={fb.att} width={250} height={50} />
                </div>
              </div>
            </div>
            {/**top banner bottom card */}

            <div className="px-2 flex flex-wrap justify-center items-start ">
              <div ref={ref1} className="keen-slider">

                {fb.packs.map((srv: any) => (
                  <div key={srv.name} className="keen-slider__slide">
                    <div className={`flex-shrink-0 m-6 relative  overflow-hidden 
     rounded-lg max-w-xs shadow-lg ${srv.col}`}>
                      <svg
                        className="absolute bottom-0 left-0 mb-8"
                        viewBox="0 0 375 283"
                        fill="none"
                        style={{ transform: "scale(1.5)", opacity: "0.1" }}
                      >
                        <rect
                          x="159.52"
                          y={175}
                          width={152}
                          height={152}
                          rx={8}
                          transform="rotate(-45 159.52 175)"
                          fill="white"
                        />
                        <rect
                          y="107.48"
                          width={152}
                          height={152}
                          rx={8}
                          transform="rotate(-45 0 107.48)"
                          fill="white"
                        />
                      </svg>
                      <div className="relative pt-10 px-10 flex items-center justify-center">
                        <div
                          className="block absolute w-48 h-48 bottom-0 left-0 -mb-24 ml-3"
                          style={{
                            background: "radial-gradient(black, transparent 60%)",
                            transform: "rotate3d(0, 0, 1, 20deg) scale3d(1, 0.6, 1)",
                            opacity: "0.2"
                          }}
                        />
                        <Image width="100" height='100'
                          className="relative w-40"
                          src={srv.img}
                          alt={srv.att}
                        />
                      </div>
                      <div className="relative text-white px-6 pb-6 mt-6">
                        <div className="flex justify-between">
                          <span className="block opacity-75 -mb-1 uppercase text-xl">{srv.name}</span>
                          <span className="block font-semibold text-xl">{srv.result}</span>
                        </div>
                        <div className={`bg-white rounded-full text-center ${srv.text} text-xl font-bold px-3  py-2 leading-none flex items-center`}>
                          {srv.prix}
                        </div>
                        <button className={`w-full rounded-2xl cursor-help shadow-2xl text-center text-2xl font-bold  uppercase mt-3 p-2
       bg-yellow-50 ${srv.text}`}>
                          buy
                        </button>
                      </div>
                    </div>

                  </div>
                ))}
              </div>
            </div></div>

        ))
      }


    </Fragment>

  )
}
