import React from 'react'
import Image from 'next/image'
import {ProcessInfo} from "@/DB/main"
import Link from 'next/link'
export default function Plan() {
  return (
    <>
    <div className='mob:h-screen mob:w-screen' ></div>
    <div id='plan' className='overflow-auto mob:overflow-y-scroll lg:flex justify-center block w-sec mx-2 -my-2  shadow-2xl bg-background-review '>
       {/**for 1st part */}
        <div className='lg:w-1/2 w-full mr-5 flex flex-col justify-center lg:-mt-10'>
        <div className='' >
        <h5  className="titleh5 pb-2">
                  ـــOur plan process
         </h5>     
         <h2  className="titleh2">
         A simple, yet effective three step process
                 </h2>
                 
        <p className='psars pb-5 '>
        At our agency, we've developed a simple, yet effective three-step process: consultation, planning, and execution. We listen to your goals, create a tailored plan, and execute it with precision to deliver exceptional results.
        </p>
        </div>
                <div className=' flex justify-center  '>
              <button className=" btn1">
                <Link href={"/contact"}
                  className="btn"
                >
                  talk to an expert
                </Link>
              </button>
              </div>
        </div>
        {/**for 2nd part */}
        <div className='flex flex-nowrap flex-col  lg:w-1/2 w-full max-h-screen mr-5 '>
          {ProcessInfo.map((p:any)=>(
        <div key={p.id} className='mt-5 rounded-3xl lg:flex block bg-white '>
            <div className='mx-5 h-auto  w-auto'>
           <Image className='rounded-3xl' title={p.title} src={p.logo} height={400}width={400} alt='plan'/>
              </div>
        <div className='m-5'>
        <h1 className='titleh2' >{p.name} </h1>
           <p className='psars'>
           {p.text}
             </p>
        </div>
        </div>
))}
        </div>
    </div>
    </>
  )
}
