import React, { useEffect, useState } from "react";
import Image from "next/image"
import Link from "next/link";
import { usePathname } from 'next/navigation';
import {navigation} from "@/DB/navbar"
export default function Navbar() {
  const currentRoute = usePathname();

  const [svg, setSvg] = useState("#ff3951");
  const [smMenu, setsmMenu] = useState("hidden");
  const [subMenu, setSubMenu] = useState("hidden space-x-5 mb-3 mt-1  py-3 lg:flex bg-gray-200 rounded-2xl ");
  const [subMenu1, setSubMenu1] = useState(" mx-5 space-y-3 my-5");
  const [children, setChildren] = useState(0);
  const [navbarBg, setNavbarBg] = useState('bg-transparent w-screen lg:w-full fixed top-[46px] left-0 pb-1 z-50');

  const handleScroll = () => {
    if (window.scrollY > 100) {
      setNavbarBg('bg-white  w-screen -mt-1 lg:w-full fixed top-[46px] left-0 pb-1  z-50'); // Add your desired background color class
    } else {
      setNavbarBg('bg-transparent w-screen lg:w-full fixed top-[46px] left-0 pb-1 z-50');
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [navbarBg]);

const linkStyle="text-lg font-semibold capitalize mt-3 leading-6 p-2 text-gray-900 hover:bg-gray-600 hover:uppercase hover:tracking-widest hover:text-secandry-light hover:rounded-full"
const activeLink="rounded-2xl capitalize mt-3 border-solid border-2 p-2 text-lg font-semibold capitalize leading-6 text-white hover:uppercase hover:tracking-widest bg-secandry-light"
  function HandelToggel(){
    if(smMenu==="hidden"){
      setsmMenu("block")
    }
    else{
      setsmMenu("hidden")
    }
  }
  useEffect(() => {
    switch (currentRoute) {
      case "/":
      setChildren(0)
      break;
      case "/services":
      setChildren(1)
      break;
      case "/packages":
      setChildren(3)
      break;
      default:
        setChildren(3)
        setSubMenu("hidden")
        setSubMenu1("hidden")
        break;
    }
  }, [currentRoute])
  
  
  return (
    <>
      
      <nav className={navbarBg }>
        {/**for logo icon lg */}
       
        {/**for LG menu */}
        <div className="lg:mx-5 lg:flex-none mt-3 mob:mx-5  flex relative justify-between">
          <div className=" hidden lg:flex flex-col">
        <div className="hidden space-x-5 py-3 lg:inline ">
            {navigation.map(({name,href}) => (
              <Link
                key={name}
                href={href}
                className={`${currentRoute==href?activeLink:linkStyle}`}
              >
                {name}
              </Link>
            ))}
            </div>
            <div className={subMenu}>
            {children<=3?
     navigation[children].children.map((children) => (
      <div className="flex px-1 " key={Math.random()} >
         <Image src={children.icon} width={32} height={32} alt={children.att} />
              <Link
                key={children.name}
                href={children.href}
                className={`${currentRoute==children.href?activeLink:linkStyle}`}
              >
                {children.name}
              </Link>
              </div>
            )):null
            
            }
            </div>
            </div>
          {/**for menu icon sm*/}
          <div className="lg:hidden">
      <button className="relative mt-5 group lg:hidden" onClick={HandelToggel}>
        <div className="relative flex overflow-hidden items-center justify-center rounded-full w-[50px] h-[50px] transform transition-all bg-secandry-light ring-0 ring-gray-300 hover:ring-8 group-focus:ring-4 ring-opacity-30 duration-200 shadow-md">
          <div className="flex flex-col justify-between w-[20px] h-[20px] transform transition-all duration-300 origin-center overflow-hidden">
            <div className="bg-white h-[2px] w-7 transform transition-all duration-300 origin-left group-focus:translate-y-6 delay-100"></div>
            <div className="bg-white h-[2px] w-7 rounded transform transition-all duration-300 group-focus:translate-y-6 delay-75"></div>
            <div className="bg-white h-[2px] w-7 transform transition-all duration-300 origin-left group-focus:translate-y-6"></div>
            <div className="absolute items-center justify-between transform transition-all duration-500 top-2.5 -translate-x-10 group-focus:translate-x-0 flex w-0 group-focus:w-12">
              <div className="absolute bg-white h-[2px] w-5 transform transition-all duration-500 rotate-0 delay-300 group-focus:rotate-45"></div>
              <div className="absolute bg-white h-[2px] w-5 transform transition-all duration-500 -rotate-0 delay-300 group-focus:-rotate-45"></div>
            </div>
          </div>
        </div>
      </button>
    </div>
          <div className="lg:grow flex justify-center lg:mr-16">
    <Link href={"/"}>
              <div className=" lg:inline ">
                <Image src="/logo.svg" className=" 
        lg:top-[60px] w-[64px] h-[64] mt-2 "  width={5} height={5} alt="logo" />
        </div>
        </Link>
        </div>
          <div className="mt-5 lg:flex-none lg:mt-5 lg:-mx-7 flex space-x-5 ">
              <div className="mt-[-23px] hidden lg:block w-52">
                <button className=" mx-4 mb-5  justify-end mt-3 h-16 w-auto rounded-full bg-secandry-light font-semibold text-red-50 drop-shadow-2xl">
                  <Link href={"/contact"}
                    className="rounded-full p-2 bg-secandry-light text-lg font-semibold capitalize
               leading-6 text-cyan-50
                 hover:uppercase"
                  >
                    talk to an expert
                  </Link>
                </button>
              </div>
        
            
            <div
              className=" h-12 w-12 rounded-lg border
               bg-slate-50 py-2 hover:bg-secandry-light
                lg:hidden"
              onMouseEnter={() => setSvg("#fff")}
              onMouseLeave={() => setSvg("#ff3951")}
            >
              <Link href={"/contact"}>
              <svg
                className="mx-1 h-8 w-8 "
                fill={svg}
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 50 50"
              >
                <path d="M18 2C9.93 2 7 7.471 7 11.148 7 16.96 12.534 20 18 20c5.333 0 11-3.167 11-9.04C29 5.77 24.374 2 18 2zm12 3c-.305 0-.604.011-.9.027a10.035 10.035 0 011.9 5.934c0 5.218-3.551 8.677-7.912 10.18C25.266 22.469 27.89 23 30 23c5.333 0 11-3.167 11-9.04C41 8.77 36.374 5 30 5zM5.5 18C3.57 18 2 19.346 2 21s1.57 3 3.5 3S9 22.654 9 21s-1.57-3-3.5-3zm39 0c-1.93 0-3.5 1.346-3.5 3s1.57 3 3.5 3 3.5-1.346 3.5-3-1.57-3-3.5-3zm-29.969 6C9.347 24 8 26.971 8 29.75c0 .676.225 1.637.387 2.242C8.176 32.324 8 32.81 8 33.5c0 1.328.57 2.04 1.271 2.336.241.88.721 1.797 1.227 2.357v1.588c-.335.577-1.329 1-2.377 1.444-2.067.875-4.895 2.072-5.119 5.714A1 1 0 004 48h42a.998.998 0 00.998-1.063c-.225-3.66-3.437-4.851-5.783-5.72-1.161-.431-2.36-.878-2.715-1.444v-1.58c.506-.56.985-1.475 1.227-2.357C40.43 35.542 41 34.829 41 33.5c0-.69-.176-1.176-.387-1.508.162-.605.387-1.566.387-2.242 0-2.133-.688-4.417-3.285-4.717l-.324-.545a.999.999 0 00-.86-.488C31.347 24 30 26.971 30 29.75c0 .676.225 1.637.387 2.242-.211.332-.387.818-.387 1.508 0 1.328.57 2.04 1.271 2.336.241.88.721 1.797 1.227 2.357v1.588c-.335.577-1.329 1-2.377 1.444-2.067.875-4.895 2.072-5.119 5.714-.001.021.008.04.008.061h-.022c0-.021.011-.041.01-.063-.225-3.66-3.437-4.851-5.783-5.72-1.161-.431-2.36-.878-2.715-1.444v-1.58c.506-.56.985-1.475 1.227-2.357C18.43 35.542 19 34.829 19 33.5c0-.69-.176-1.176-.387-1.508.162-.605.387-1.566.387-2.242 0-2.133-.688-4.417-3.285-4.717l-.324-.545a.999.999 0 00-.86-.488zM3.5 26A1.5 1.5 0 002 27.5 1.5 1.5 0 003.5 29 1.5 1.5 0 005 27.5 1.5 1.5 0 003.5 26zm43 0a1.5 1.5 0 00-1.5 1.5 1.5 1.5 0 001.5 1.5 1.5 1.5 0 001.5-1.5 1.5 1.5 0 00-1.5-1.5z" />
              </svg>
              </Link>
            </div>
          </div>
        </div>
              {/** * SM Menu  */}
      <div className={smMenu}>
      <div  className=" bg-white -mt-5 " >
            <div className="flex flex-col">
        <div className="flex flex-nowrap ml-6 mt-5 max-w-xs ">
            {navigation.map(({name,href}) => (
              <Link
                key={name}
                href={href}
                className={`${currentRoute==href?activeLink:linkStyle}`}
              >
                {name}
              </Link>
            ))}
            </div>
            <div className={subMenu1}>
            {children<=3?
     navigation[children].children.map((children) => (
      <div className="flex px-1 " key={Math.random()}>
           <Image src={children.icon} width={40} height={32} alt={children.att}/>
              <Link
                key={children.name}
                href={children.href}
                className={`${currentRoute==children.href?activeLink:linkStyle}`}
              >
                {children.name}
              </Link>
              </div>
            )):null
            
            }
            </div>
            </div>       
      </div></div>
      </nav>

    </>
  );
};
