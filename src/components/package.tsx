import React from 'react'
import Image from "next/image"
import {PackageInfo} from "@/DB/main"
import { useKeenSlider } from "keen-slider/react"
import "keen-slider/keen-slider.min.css"

export default function Package() {
  const [ref] = useKeenSlider<HTMLDivElement>({
     loop: false,
    mode: "snap",
    rtl: false,
    slides: { perView: "auto",spacing:15 },
  })
  return (
    <div className='w-sec mx-5 rounded-3xl shadow-2xl  mt-20'>
      <div className='flex flex-row  justify-center'>       
      <div>
        <h5  className="text-xl  text-center font-semibold capitalize leading-6 text-secandry-light">
                  ـــOur plan price
         </h5>     
         <h2  className="text-3xl text-center font-semibold capitalize leading-10 text-gray-900 pt-6">
         pricing plans for every need
          </h2>
        <p className=' leading-10 text-gray-900 text-center text-xl mb-5'>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.  facilis!
        </p></div>
        </div>
        {/**::secand part:: */}
        <div ref={ref} className='flex w-full rounded-3xl bg-gray-100'>
          { PackageInfo.map((p)=>(
     <div key={Math.random()} className='keen-slider__slide  overflow-hidden'>
     <div className=' rounded-xl lg:w-customPmp mob:w-full'>
       <div className='border-collapse border-r-4'>
         <div className='flex justify-start'>
       <Image className='mx-5 pt-5 px-2 rounded-2xl justify-center' src={p.logo} width={100} height={100} alt='aze'/>
       </div>
       <div className='flex justify-start'>
       <h3 className='mx-5 pt-5 text-xl capitalize leading-7 text-gray-400
        hover:text-secandry-light'>{p.title}</h3>
   </div>
   <div  className=' flex justify-start py-3'>
    <h1 className='mx-5 text-2xl leading-7 text-blue-950 '>{p.prix}</h1>
    </div>
   <div className=' flex justify-start'>
    <p className='mx-5 text-xl capitalize leading-7 text-gray-400'>{p.text}</p>
    </div>
    <div className='flex justify-start py-3'>
    <h1 className='mx-5 text-2xl leading-7 text-blue-950 capitalize '> what include ?</h1>
    </div>
    {p.benefit.map((ar:any)=>(
    <div className='flex justify-start' key={Math.random()}>
     <Image className='mx-5 pt-3' src={p.okicon} width={25} height={25} alt="z"/>
     <h5 className='pt-3 text-xl '> {ar}</h5>
    </div>
    ))}
    <div className='flex justify-start py-3'>
    <button className=" m-4 mt-1 h-20 w-52  rounded-full bg-secandry-light font-semibold text-red-50 drop-shadow-2xl">
             <a
               className="rounded-full justify-center bg-secandry-light text-lg font-semibold capitalize
            leading-6   text-cyan-50
              hover:uppercase"
             >
               more details
             </a>
           </button> 
       </div>
       </div>

       
       </div>
       </div>
          ))
       
        }

          </div>

    </div>
  )
}
