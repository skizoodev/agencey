import { useKeenSlider } from "keen-slider/react";
import { Ads } from "@/DB/main";

export default function Adsh() {
  const [sliderRef] = useKeenSlider<HTMLDivElement>(
    {
      loop: true,
    },
    [
      (slider) => {
        let timeout: ReturnType<typeof setTimeout>;
        let mouseOver = false;
        function clearNextTimeout() {
          clearTimeout(timeout);
        }
        function nextTimeout() {
          clearTimeout(timeout);
          if (mouseOver) return;
          timeout = setTimeout(() => {
            slider.next();
          }, 5000);
        }
        slider.on("created", () => {
          slider.container.addEventListener("mouseover", () => {
            mouseOver = true;
            clearNextTimeout();
          });
          slider.container.addEventListener("mouseout", () => {
            mouseOver = false;
            nextTimeout();
          });
          nextTimeout();
        });
        slider.on("dragStarted", clearNextTimeout);
        slider.on("animationEnded", nextTimeout);
        slider.on("updated", nextTimeout);
      },
    ]
  );

  return (
    <div className=" h-12  w-screen lg:w-full bg-gray-800 fixed top-0 bottom-0 left-0 right-0 z-50">
      <div ref={sliderRef} className="keen-slider">
        {Ads.map((t) => (
          <div key={t.id} className="keen-slider__slide">
            <p className="mx-2 overflow-hidden whitespace-wrap  pt-2 text-center align-middle text-sm	font-bold	uppercase text-yellow-100 lg:text-2xl	">
              {t.text}
            </p>
          </div>
        ))}
      </div>
    </div>
  );
}
