import { useKeenSlider } from "keen-slider/react";
import { HeroImg } from "@/DB/main";
import Link from 'next/link'
import Image from "next/image";
export default function Hero() {  
  const [sliderRef] = useKeenSlider<HTMLDivElement>(
    {
      loop: true,
    },
    [
      (slider) => {
        let timeout: ReturnType<typeof setTimeout>;
        let mouseOver = false;
        function clearNextTimeout() {
          clearTimeout(timeout);
        }
        function nextTimeout() {
          clearTimeout(timeout);
          if (mouseOver) return;
          timeout = setTimeout(() => {
            slider.next();
          }, 5000);
        }
        slider.on("created", () => {
          slider.container.addEventListener("mouseover", () => {
            mouseOver = true;
            clearNextTimeout();
          });
          slider.container.addEventListener("mouseout", () => {
            mouseOver = false;
            nextTimeout();
          });
          nextTimeout();
        });
        slider.on("dragStarted", clearNextTimeout);
        slider.on("animationEnded", nextTimeout);
        slider.on("updated", nextTimeout);
      },
    ]
  );
  const stars=[]
  for (let i = 0; i < 5; i++) {
    stars.push(  
    <div key={Math.random()}>
    <svg
      className="w-4 h-4 text-yellow-300"
      aria-hidden="true"
      xmlns="http://www.w3.org/2000/svg"
      fill="currentColor"
      viewBox="0 0 22 20"
    >
      <path d="M20.924 7.625a1.523 1.523 0 0 0-1.238-1.044l-5.051-.734-2.259-4.577a1.534 1.534 0 0 0-2.752 0L7.365 5.847l-5.051.734A1.535 1.535 0 0 0 1.463 9.2l3.656 3.563-.863 5.031a1.532 1.532 0 0 0 2.226 1.616L11 17.033l4.518 2.375a1.534 1.534 0 0 0 2.226-1.617l-.863-5.03L20.537 9.2a1.523 1.523 0 0 0 .387-1.575Z" />
    </svg></div>)
    ;
  }
  return (
    <>   
 <div className='mt-48'></div>

  {/* Hero */}
      <div className="max-w-[85rem] mx-auto px-4 sm:px-6 lg:px-8">
    {/* Grid */}
    <div className="grid md:grid-cols-2 gap-4 md:gap-8 xl:gap-20 md:items-center">
      <div>
        <h1 className="block text-3xl capitalize font-bold text-gray-800 sm:text-4xl lg:text-6xl lg:leading-tight ">
          grow your business with <span className="text-secandry-light">Sarsabs</span> Team
        </h1>
        <p className="psars">
                   Stay ahead of the competition with our creative strategies and cutting-edge campaigns. Our team thrives on innovation, delivering unique and impactful experiences that set your brand apart.   
        </p>
           {/* Review */}
           <div className="mt-2 grid grid-cols-2 gap-x-5">
          {/* Review */}
          <div className="py-5">
          <div className="flex items-center space-x-1">
{stars}
</div>

            <p className="mt-3 text-lg text-gray-800">
              <span className="font-bold ">4.6</span> /5 - from 54 reviews
            </p>
            <div className="mt-5">
              {/* Star */}
            <Image src={"/asset/home/facebook.gif" } width={39} height={39} alt="ez"/>
              {/* End Star */}
            </div>
          </div>
          {/* End Review */}
          {/* Review */}
          <div className="py-5">
          <div className="flex items-center space-x-1">

  {stars}
</div>

            <p className="mt-3 text-lg text-gray-800">
              <span className="font-bold ">4.8</span> /5 - from 5 reviews
            </p>
            <div className="mt-5">
              {/* Star */}
              <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 48 48" width="39px" height="39px" baseProfile="basic"><path fill="#00b67a" d="M45.023,18.995H28.991L24.039,3.737l-4.968,15.259L3.039,18.98l12.984,9.44l-4.968,15.243 l12.984-9.424l12.968,9.424L32.055,28.42L45.023,18.995z"/><path fill="#005128" d="M33.169,31.871l-1.114-3.451l-8.016,5.819L33.169,31.871z"/></svg>
              {/* End Star */}
            </div>
          </div>
          {/* End Review */}
        </div>
        {/* End Review */}
        {/* Buttons */}
        <div className="mt-2 grid gap-3 w-full sm:inline-flex">
          <Link href="/services"
            className="inline-flex justify-center m-3  items-center gap-x-3 text-center
             bg-secandry-light hover:uppercase border border-transparent text-lg
              text-white font-semibold rounded-2xl shadow-2xl focus:outline-none 
            focus:ring-2 focus:ring-offset-2 focus:ring-offset-white transition py-3 px-4"
          >
            Browse Services
            <svg
              className="w-2.5 h-2.5"
              width={16}
              height={16}
              viewBox="0 0 16 16"
              fill="none"
            >
              <path
                d="M5.27921 2L10.9257 7.64645C11.1209 7.84171 11.1209 8.15829 10.9257 8.35355L5.27921 14"
                stroke="currentColor"
                strokeWidth={2}
                strokeLinecap="round"
              />
            </svg>
          </Link>
          <Link
            className="inline-flex justify-center items-center gap-x-3.5 text-secandry-light m-3 text-lg 
             text-center border
             hover:border-gray-300 shadow-red-600 font-bold rounded-2xl 
             capitalize   focus:outline-none focus:ring-2
              focus:ring-gray-400 focus:ring-offset-2 focus:ring-offset-white transition py-3 px-4 hover:uppercase
               "
            href="/contact"
          >
talk to an expert          </Link>
        </div>
        {/* End Buttons */}
     
      </div>
      {/* End Col */}
          <div className="relative mx-auto  mob:hidden border-gray-800 bg-gray-800 border-[14px] rounded-[2.5rem] h-[454px] max-w-[341px] md:h-[682px] md:max-w-[512px]">
  <div className="h-[32px] w-[3px] bg-gray-800  absolute -left-[17px] top-[72px] rounded-l-lg" />
  <div className="h-[46px] w-[3px] bg-gray-800  absolute -left-[17px] top-[124px] rounded-l-lg" />
  <div className="h-[46px] w-[3px] bg-gray-800  absolute -left-[17px] top-[178px] rounded-l-lg" />
  <div className="h-[64px] w-[3px] bg-gray-800  absolute -right-[17px] top-[142px] rounded-r-lg" />
  <div className="rounded-[2rem] overflow-hidden h-[426px] md:h-[654px] bg-white ">
  <div ref={sliderRef} className="keen-slider">
        {HeroImg.map((t) => (
          <div key={t.id} className="keen-slider__slide">
              <img
                src={t.img}
      className=" h-[426px] md:h-[654px]"
      alt=""
    />
          </div>
        ))}
      </div>

  </div>
</div>



      {/* End Col */}
    </div>
    {/* End Grid */}
  </div>
  {/* End Hero */}

    </>

  )
}
