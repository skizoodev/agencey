import React from 'react'
import Image from "next/image"
export default function Header() {
  return (
 <>
 <div className='mt-48'></div>

<div className='block lg:flex mb-2 mt-12  w-screen lg:w-secads space-x-5 font-bold'>
  {/**for text */}
    <div className='w-full lg:w-1/2  '>
      <h5  className="titleh5 pb-5">
      ـــSmall Packages Solutions 
     </h5>     
     <h2  className="titleh2">
     boost your social presence
          </h2>
     <p className='psars'>
     Are you looking to enhance your social media presence and increase your Profile
      engagement? Look no further! Our premium package is the perfect solution to boost your
       profile's visibility and attract more engagement.</p>
       <p className='text-lg font-semibold  capitalize leading-10 ml-3 mr-2 pb-5 text-secandry-light '>
    Note: The results and price of our package may vary by up to 90% based on your niche and country, as well as your specific needs. We tailor our services to ensure the best outcomes for your unique requirements.
        </p>
             <div className='flex justify-between my-5'>
      <button className=" btn1">
            <a
              className="btn"
            >
              talk to an expert
            </a>
          </button>

        </div>    
       </div>
  {/**for img */}
    <div className="bg-cover w-auto my-5 py-5 lg:w-half mob:hidden ">
    <Image className='absolute z-10' src ="/asset/service/bg.jpeg" width={500} height={200} alt='icons'/>
<Image className='absolute z-10 ml-[-45px] mt-[-36px] lg:mx-36' src ="/asset/service/hero.svg" width={500} height={200} alt='icons'/>
<Image className='relative z-10  ' src ="/asset/service/men.svg" width={200} height={100} alt='dashbord'  />
    </div>
</div>
 </>
    
)
}
