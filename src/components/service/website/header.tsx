import Image from 'next/image'
export default function Header() {
  return (
<>
<div className='mt-48'></div>

<div className="max-w-[85rem] mx-auto px-4 sm:px-6 lg:px-8">
    {/* Grid */}
    <div className="grid lg:grid-cols-7 lg:gap-x-8 xl:gap-x-12 lg:items-center">
      <div className="lg:col-span-3">
        <h1 className="block text-3xl font-bold text-gray-800 sm:text-4xl md:text-5xl lg:text-6xl">
        Your Digital Future Well Built
        </h1>
        <p className="mt-3 text-lg text-gray-800">
        Embracing a professional website can be a pivotal turning point for your business in today's digital era, opening doors to new opportunities and enhanced online presence    </p>
        <div className="mt-5 lg:mt-8 flex flex-col items-center gap-2 sm:flex-row sm:gap-3">
          <div className="w-full sm:w-auto">
            <label htmlFor="hero-input" className="sr-only">
              Search
            </label>
            <input
              type="text"
              id="hero-input"
              name="hero-input"
              className="py-3 px-4 block w-full xl:min-w-[18rem] border-gray-200 shadow-sm rounded-md focus:z-10 focus:border-blue-500 focus:ring-blue-500"
              placeholder="Enter work email"
            />
          </div>
          <button
            className="w-full sm:w-auto inline-flex justify-center items-center
             gap-x-3 text-center bg-secandry-light
              hover:bg-red-600 border
               border-transparent text-white
                font-medium rounded-md focus:outline-none 
                focus:ring-2 focus:ring-secandry-light focus:ring-offset-2 focus:ring-offset-white transition py-3 px-4
             "
          >
            Request demo
          </button>
        </div>
        {/* Brands */}
     
      </div>
      {/* End Col */}
      <div className="lg:col-span-4 mt-10 lg:mt-0">
        <Image width={1000} height={1000} 
          className="w-full rounded-xl"
          src="/asset/service/web/hero.jpg"
          alt="website development"
        />
      </div>
      {/* End Col */}
    </div>
    {/* End Grid */}
  </div>
  {/* End Hero */}
</>

  )
}
