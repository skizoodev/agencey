import {backEnd} from '@/DB/website'
import Image from 'next/image'
export default function Dev() {
  return (
<>
<div className='mt-48'></div>

<div className="max-w-[85rem] mx-auto px-4 sm:px-6 lg:px-8">
    {/* Grid */}
    <div className="grid lg:grid-cols-7 lg:gap-x-8 xl:gap-x-12 lg:items-center">
      <div className="lg:col-span-3">
        <h1 className="block text-lg font-bold text-gray-800 sm:text-4xl md:text-5xl lg:text-3xl">
        Cutting-edge backend development tools for seamless user experiences.
        </h1>
        <p className="mt-3 text-xl font-sarsabs  text-gray-800">
        Playing with high-end and versatile backend solutions, we’ll produce complex, secure and scalable architecture & software according to the industry’s best standards.
        </p>
      
        {/* Brands */}
         {/* List */}
         <ul role="list" className="space-y-2 sm:space-y-4 mt-3">
          {backEnd.map((i)=>(
 <li className="flex space-x-3" key={Math.random()}>
 {/* Solid Check */}
 <Image src={"/asset/emailOk.svg"} width={24} height={24} alt='done'/>
 {/* End Solid Check */}
 <span className="text-sm sm:text-base text-gray-500">
   <span className="font-bold">{i}</span> </span>
</li>
          ))}
           
          
          </ul>
          {/* End List */}
      </div>
      {/* End Col */}
      <div className="lg:col-span-4 mt-10 lg:mt-0">
        <Image width={1000} height={1999}
          className="w-full rounded-xl"
          src="/asset/service/web/hero.jpg"
          alt="website development"
        />
      </div>
      {/* End Col */}
    </div>
    {/* End Grid */}
  </div>
  {/* End Hero */}
</>

  )
}
