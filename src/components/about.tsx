import Image from "next/image"
import {Abouts} from "@/DB/main"
export default function About() {
  return (
<>
  {/* Features */}
  <div className="max-w-[85rem] px-4 py-10 sm:px-6 lg:px-8 lg:py-14 mx-auto" id="about">
    {/* Grid */}
  
    <div className="md:grid md:grid-cols-2 md:items-center md:gap-12 xl:gap-32">
      <div className="bg-cover" style={{ backgroundImage: 'url(/asset/about/bg.jpeg)' }}>
          <Image
          className="relative z-10"width={429} height={327}
          src="/asset/about/person.svg"
          alt="Image Description"
        />
        <Image
          className="rounded-xl relative z-20 -top-32"width={429} height={327}
          src="/asset/about/dashbord.svg"
          alt="Image Description"
        />
   
      </div>
      {/* End Col */}
      <div className="mt-2 sm:mt-10 lg:-mt-[10rem]">
 
        <div className="space-y-6 sm:space-y-8">
            
          {/* Title */}
          <div className="space-y-2 md:space-y-4">
          <h5  className="titleh5 ">
                  ـــWhy SarSabs Agency

         </h5> 
            <h2 className="titleh2">
            Why working with us?
            </h2>
            <p className='psars '>
              Besides working with start-up enterprises as a partner for
              digitalization, we have built enterprise products for common pain
              points that we have encountered in various products and projects.
            </p>
          </div>
          {/* End Title */}
          {/* List */}
          <ul role="list" className="space-y-2 sm:space-y-4">
            {Abouts.map((a)=>(
 <li key={a.id} className="flex space-x-3">
 {/* Solid Check */}
 <Image className='rounded-3xl' alt='icon' width={30} height={30} src={"/asset/service/emailOk.svg"} />

 {/* End Solid Check */}
 <span className="text-lg sm:text-base text-gray-500">
   <span className="font-bold">{a.text}</span>
 </span>
</li>
            ))}
           
          </ul>
          {/* End List */}
        </div>
      </div>
      {/* End Col */}
    </div>
    {/* End Grid */}
  </div>
  {/* End Features */}
</>
  )
}
