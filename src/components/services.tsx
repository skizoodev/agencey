import {useState,useEffect} from 'react' 
import MB from "@/scene/mb"
import LG from "@/scene/lg"
export default function Services() {
  const [deviceWidth, setDeviceWidth] = useState(0);
  useEffect(() => {
    const handleResize = () => {
      setDeviceWidth(window.innerWidth);
    };

    // Initial device width
    setDeviceWidth(window.innerWidth);

    // Add event listener for window resize
    window.addEventListener('resize', handleResize);

    // Clean up the event listener on component unmount
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [deviceWidth]);
  return (
    <div id='service' className='w-sec bg-background-review m-2 rounded-3xl shadow-2xl mob '>
        {/**for text head */}
        <div className='max-w-2xl ml-auto mr-auto pt-12'>
        <h5  className="titleh5 mb-3">
                  ـــHigh-impact business services
         </h5>     
         <h2  className="titleh2 mob:pb-5">
         we cover all aspects of business providing a comprehensive solution.
        </h2>
        </div>
 {/**for service md */}
 {
 (deviceWidth<690) ?<MB/> : <LG/>
 }

<div>
</div>
    </div>
  )
}
