import Image from 'next/image'
import React, { useState } from "react"
import { useKeenSlider } from "keen-slider/react"
import "keen-slider/keen-slider.min.css"
import {TestimonialData} from "@/DB/main"
export default function Testimonial() {
  const [currentSlide, setCurrentSlide] = React.useState(0)
  const [loaded, setLoaded] = useState(false)
  const [sliderRef, instanceRef] = useKeenSlider<HTMLDivElement>({
    initial: 0,
    slideChanged(slider) {
      setCurrentSlide(slider.track.details.rel)
    },
    created() {
      setLoaded(true)
    },
  })
  return (
    <div id='testimonial' className=' bg-background-review rounded-3xl shadow-2xl w-sec m-2 py-5'>
        {/**for text head */}
        <div className='max-w-xl ml-auto mr-auto pt-12'>
        <h5  className="titleh5">
                  ـــTestimonials
         </h5>     
         <h2  className="titleh2">
         hear what our amazing customers say
           </h2>
        </div>
 {/**for service */}
 <div className="navigation-wrapper">
 <div ref={sliderRef} className="keen-slider">
  {TestimonialData.map((t)=>(
    <div key={t.id} className='keen-slider__slide number-slide1'>
 <div className=" bg-white rounded-3xl mx-5 my-10 lg:my-10  lg:mx-48  ">
 <h4 className='font-bold text-3xl text-center lg:text-4xl text-secandry-light mr-2 capitalize ml-5 mt-3 pt-5 '>{t.title}</h4>
 <div className='block lg:flex'>
    <div  className='w-full '>
       <p className='psars'>{t.text}</p> 
    </div>
    <div className=' flex justify-end mx-2 w-full h-full '>
       <Image className='lg:mt-20 ' src={t.logo} width={300} height={300} alt='compnay'/> 
</div>

 </div>
 <h4 className='titleh2'>{t.name}</h4>
       <h3 className='titleh5 pb-5'>{t.company}</h3>
</div>
</div>
  ))}

</div>

</div>
{loaded && instanceRef.current && (
        <div className="dots">
          {[
            ...Array(instanceRef.current.track.details.slides.length).keys(),
          ].map((idx) => {
            return (
              <button
                key={idx}
                onClick={() => {
                  instanceRef.current?.moveToIdx(idx)
                }}
                className={"dot" + (currentSlide === idx ? " active" : "")}
              ></button>
            )
          })}
        </div>
      )}
      <div className='flex justify-center'>
<button className="h-20 w-52  rounded-full bg-secandry-light font-semibold text-red-50 drop-shadow-2xl">
                <a
                  className="rounded-full bg-secandry-light text-lg font-semibold capitalize
               leading-6   text-cyan-50
                 hover:uppercase"
                >
                  talk to an expert
                </a>
 </button>
 </div>
         </div>
  )
}
